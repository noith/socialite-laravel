<?php

use Illuminate\Support\Facades\Route;
use Noith\Socialite\Http\Controllers\SocialiteController;

Route::prefix('socialite')->name('socialite.')->middleware('web')->group(function () {
    Route::any('{provider}/callback', [SocialiteController::class, 'handle'])->name('handle');
    Route::get('{provider}', [SocialiteController::class, 'redirect'])->name('redirect');
});
