<?php
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Noith\Socialite\Contracts\SocialiteProviderEnumContract;
use Noith\Socialite\Models\SocialiteUser;

return [
    'user_model' => User::class,
    'socialite_user_model' => SocialiteUser::class,
    'provider_enum' => SocialiteProviderEnumContract::class,
    'redirect' => RouteServiceProvider::HOME,
];
