<?php

namespace Noith\Socialite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Noith\Socialite\Events\AuthenticatedEvent;
use Noith\Socialite\Services\SocialiteAuthService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SocialiteController extends Controller
{
    public function __construct()
    {
        $enum = config('socialite.provider_enum');
        if ($enum) {
            foreach ($enum::cases() as $service) {
                config()->set('services.' . $service->value . '.redirect', route('socialite.handle', [
                    'provider' => $service->value,
                ]));
            }
        }
    }

    public function redirect(string $provider): \Symfony\Component\HttpFoundation\RedirectResponse|\Illuminate\Http\RedirectResponse
    {
        return SocialiteAuthService::redirect($provider);
    }

    public function handle(Request $request, string $providerId)
    {
        try {
            $service = SocialiteAuthService::make($providerId);
            $socialiteUser = $service->findOrCreate();

            Auth::login($socialiteUser->user);

            AuthenticatedEvent::dispatch($socialiteUser);

            return redirect(config('socialite.redirect'));
        } catch (\Throwable $ex) {
            Log::error($ex->getMessage());
            throw new NotFoundHttpException('No such oauth provider');
        }
    }
}
