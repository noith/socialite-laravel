<?php

namespace Noith\Socialite\Providers;

use Illuminate\Support\ServiceProvider;

class SocialiteAuthServiceProvider extends ServiceProvider
{

    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/../../database/migrations' => database_path('migrations')
        ], 'socialite-migrations');
        $this->publishes([
            __DIR__ . '/../../config' => config_path()
        ], 'socialite-config');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/routes.php');
    }
}
