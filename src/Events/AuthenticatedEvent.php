<?php

namespace Noith\Socialite\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Noith\Socialite\Models\SocialiteUser;

class AuthenticatedEvent
{
    use Dispatchable, SerializesModels;

    /**
     * Create a new event instance.
     */
    public function __construct(
        public SocialiteUser $user,
    )
    {
    }
}
