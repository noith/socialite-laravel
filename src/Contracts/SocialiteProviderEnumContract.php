<?php

namespace Noith\Socialite\Contracts;

interface SocialiteProviderEnumContract
{
    public function supportHeadless(): bool;
}
