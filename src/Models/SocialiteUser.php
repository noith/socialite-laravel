<?php

namespace Noith\Socialite\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SocialiteUser extends Model
{
    protected $fillable = [
        'user_id',
        'provider',
        'provider_user_id',
        'payload',
    ];

    protected $casts = [
        'payload' => 'array',
    ];

    public function __construct(array $attributes = [])
    {
        $this->mergeCasts([
            'provider' => config('socialite.provider_enum')
        ]);
        parent::__construct($attributes);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(config('socialite.user_model'), 'user_id');
    }

}
