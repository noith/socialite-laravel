<?php

namespace Noith\Socialite\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\AbstractUser;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\User;
use Noith\Socialite\Contracts\SocialiteProviderEnumContract;
use Noith\Socialite\Events\SocialiteUserCreatedEvent;
use Noith\Socialite\Events\UserCreatedEvent;
use Noith\Socialite\Models\SocialiteUser;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SocialiteAuthService
{
    protected function __construct(
        public SocialiteProviderEnumContract $provider,
        public User                          $socialiteUser
    )
    {
    }

    public static function make(SocialiteProviderEnumContract|string $provider, ?AbstractUser $user = null): static
    {
        if (!($provider instanceof SocialiteProviderEnumContract)) {
            $provider = static::providerFromString($provider);
        }
        if (is_null($user)) {
            $user = $provider->supportHeadless()
                ? Socialite::driver($provider->value)->stateless()->user()
                : Socialite::driver($provider->value)->user();
        }
        return new static($provider, $user);
    }

    private static function providerFromString(string $provider): SocialiteProviderEnumContract
    {
        $enum = config('socialite.provider_enum');
        try {
            return $enum::from($provider);
        } catch (\Throwable $e) {
            throw new NotFoundHttpException();
        }
    }

    public static function redirect(string $provider)
    {
        static::providerFromString($provider);
        return Socialite::driver($provider)->redirect();
    }

    public function exists(): ?SocialiteUser
    {
        $model = config('socialite.socialite_user_model');
        return $model::where('provider', '=', $this->provider)
            ->where('provider_user_id', $this->socialiteUser->getId())
            ->first();
    }

    public function findOrCreate(): SocialiteUser
    {
        $socialUser = $this->exists();
        if ($socialUser) {
            return $socialUser;
        }
        DB::transaction(function () use (&$socialUser) {
            $userModel = config('socialite.user_model');
            $user = $userModel::where('email', $this->socialiteUser->getEmail())->first();
            if (!$user) {
                $user = $userModel::create([
                    'name' => $this->socialiteUser->getName() ?? $this->socialiteUser->getNickname() ?? $this->socialiteUser->getEmail(),
                    'email' => $this->socialiteUser->getEmail() ?? $this->socialiteUser->getId() . '@' . $this->provider->value,
                    'password' => Hash::make(Str::random(8)),
                ]);
                UserCreatedEvent::dispatch($user);
            }
            $socialUserModel = config('socialite.socialite_user_model');
            $socialUser = $socialUserModel::create([
                'provider' => $this->provider,
                'provider_user_id' => $this->socialiteUser->getId(),
                'payload' => $this->socialiteUser,
                'user_id' => $user->id
            ]);
            SocialiteUserCreatedEvent::dispatch($socialUser);
        });
        return $socialUser;

    }
}
